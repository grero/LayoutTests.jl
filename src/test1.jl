using Makie
import AbstractPlotting: pixelarea
using Test

function test1()
	main_scene = Scene(resolution=(800,600))

	scene1 = Scene(main_scene)
	scene2 = Scene(main_scene)

	AbstractPlotting.scale!(scene1, 400,300)
	AbstractPlotting.translate!(scene1, 0, 0)

	AbstractPlotting.scale!(scene2, 400, 300)
	AbstractPlotting.translate!(scene2, 0, 300)

	AbstractPlotting.update_cam!(scene1)
	AbstractPlotting.update_cam!(scene2)

	scatter!(scene1, rand(10), rand(10),color="red")[end]
	scatter!(scene2, rand(10), rand(10), color="blue")[end]
	AbstractPlotting.update!(main_scene)
	main_scene
end

function test2(;_padding=0.01)
	main_scene = Scene(resolution=(800,600))

	padding = map(pixelarea(main_scene)) do hh
		round(Int64, _padding*maximum(hh.widths))
	end
	area1 = map(pixelarea(main_scene)) do hh
		pad = padding[]
		w = 2*div(hh.widths[1]-3*pad,3)
		h = div(hh.widths[2]-3*pad,2)
		FRect(Point2f0(pad, pad), Point2f0(w,h))
	end

	area2 = map(pixelarea(main_scene)) do hh
		pad = padding[]
		w = 2*div(hh.widths[1]-3*pad,3)
		h = div(hh.widths[2]-3*pad,2)
		FRect(Point2f0(pad, h+2*pad), Point2f0(w,h))
	end
	
	area3 = map(pixelarea(main_scene)) do hh
		pad = padding[]
		w = div(hh.widths[1]-3*pad,3)
		h = hh.widths[2] - 2*pad
		FRect(Point2f0(2*w+2*pad, pad), Point2f0(w,h))
	end
	@test area1[].widths[2] + area2[].widths[2] + padding[] ≈ area3[].widths[2]
	scene1 = Scene(main_scene, area1)
	scene2 = Scene(main_scene, area2)
	scene3 = Scene(main_scene, area3)
	#manually set the resolution of the 3rd scene
	ratio = area3[].widths[2]/area1[].widths[2]
	scatter!(scene1, rand(10), rand(10),color="red")[end]
	scatter!(scene2, rand(10), rand(10), color="blue")[end]
	scatter!(scene3, rand(10), rand(10), color="orange")[end]
	for scene in [scene1, scene2, scene3]
		scene.padding[] = 0.0
		scene.scale_plot[] = true
		scene.center[] = true
		AbstractPlotting.update_limits!(scene, FRect(Point2f0(-1.0, -1.0), Point2f0(2.0, 2.0)))
		AbstractPlotting.update!(scene)
	end
	b2 = boundingbox(scene2)
	b3 = boundingbox(scene3)
	p2 = scene2.camera.projection[]
	p3 = scene3.camera.projection[]
	#no idea why this is necessary
	fudge = 0.98
	rr = p2[2,2]/(p3[2,2]*ratio)*fudge
	#create an appropriate scaling matrix
	ss = AbstractPlotting.StaticArrays.SArray{Tuple{4,4}, Float32, 2, 16}(Diagonal([1.0, rr, 1.0, 1.0]))
	p3 = ss*p3
	#re-center and re-scale
	scene3.center[] = false
	main_scene.children[3].camera.view[] = scene1.camera.view[]
	main_scene.children[3].camera.projection[] = p3
	main_scene
end
